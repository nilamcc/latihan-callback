//1. buat array of object students ada 5 data cukup
const students = [
    {
        name: "Nilam",
        age : 20,
        province: "lampung",
        single: false,
    },
    {
        name: "Haekal",
        age : 18,
        province: "banten",
        single: false,
    },
    {
        name: "Anggia",
        age : 22,
        province: "jakarta",
        single: false,
    },
    {
        name: "Nadia",
        age : 21,
        province: "lampung",
        single: true,
    },
    {
        name: "Salma",
        age : 22,
        province: "jawa barat",
        single: true,
    },
];

//2. 3 function, 1 function nentuin provinsi kalian ada di lampung atau tidak, umur kalian diatas 22 atau tidak. dan status 
function checkLocation(province) {
    tenary = (province == "lampung" ? true : false)
    return tenary;
}
function checkAge(age) {
    tenary = (age <= 21 ? true : false)
    return tenary
}
function checkSingle(single) {
    tenary = (single != true ? true : false)
    return tenary;
}

//3. callback function untuk print hasil proses 3 function diatas
function result(students, callbackLocation, callbackAge, callbackSingle) {
    let checkAll = `Nama saya ${students.name}`;
    checkAll += callbackLocation(students.province) ? " asal dari provinsi Lampung" : " bukan asal dari provinsi Lampung";
    checkAll += callbackAge(students.age) ? " umur saya kurang dari 22 tahun" : " umur saya lebih dari 22 tahun";
    checkAll += callbackSingle(students.single) ? " dan saya sudah sold out" : " dan saya masih single";
    return checkAll;
}
for (i=0; i<students.length; i++){
    console.log(result(students[i], checkLocation, checkAge, checkSingle));
}