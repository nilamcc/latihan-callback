//1. buat array of object students ada 5 data cukup
const students = [
    {
        name: "Nilam",
        age : 20,
        province: "lampung",
        single: false,
    },
    {
        name: "Haekal",
        age : 18,
        province: "banten",
        single: false,
    },
    {
        name: "Anggia",
        age : 22,
        province: "jakarta",
        single: false,
    },
    {
        name: "Nadia",
        age : 21,
        province: "lampung",
        single: true,
    },
    {
        name: "Salma",
        age : 22,
        province: "jawa barat",
        single: true,
    },
];

//2. 3 function, 1 function nentuin provinsi kalian ada di lampung atau tidak, umur kalian diatas 22 atau tidak. dan status 
function checkLocation(province) {
    tenary = (province == "lampung" ? province : "luar lampung")
    return tenary;
}
function checkAge(age) {
    tenary = (age < 22 ? "kurang dari 22" : "22 atau lebih dari 22")
    return tenary
}
function checkSingle(single) {
    tenary = (single != true ? "sudah sold out" : "single")
    return tenary;
}

//3. callback function untuk print hasil proses 3 function diatas
function result(callbackLocation, callbackAge, callbackSingle) {
    for (let i=0; i<students.length; i++) {
        location = callbackLocation(students[i].province);
        age = callbackAge(students[i].age);
        single = callbackSingle(students[i].single);
        if (location == "lampung" && age == "kurang dari 22" && single == "sudah sold out"){
            console.log(
                `Nama saya ${students[i].name}, asal dari provinsi ${location}, umur saya ${age} tahun, dan saya ${single}!`
            );
        }
    }
}
result(checkLocation, checkAge, checkSingle);
